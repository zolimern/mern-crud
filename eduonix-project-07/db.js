const mongoose = require('mongoose')
const asyncHandler = require('./middleware/asyncHandler')



module.exports = asyncHandler(async () => {

   


    const connection = await mongoose.connect(process.env.DB)

    connection ? console.log('connected to database') : console.log('could not connect to DB')

})