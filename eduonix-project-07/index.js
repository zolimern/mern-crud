require('dotenv').config()
const express = require('express')
const app = express()
const cors = require('cors')
const connection = require('./db')

const employees = require('./routes/employees')
connection()


app.use(express.json())
app.use(cors())

app.use('/api/employees', employees)

const port = process.env.PORT || 5000
app.listen(port, () => console.log(`Listening on port ${port}`))